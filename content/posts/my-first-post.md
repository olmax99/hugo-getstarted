---
title: "AWS S3 Static Sites"
date: 2021-08-08T08:09:37Z
draft: true
---

### 1. Launch a website on EC2

Ensure that the following requirements are in place:
- VPC + igw
- Subnets (public|private) in CIDR blocks
- Security Groups (minimum: open 22, 80)

Log into the new instance and provide minimum configuration:

```
$ sudo yum update -y
$ sudo yum install httpd
$ sudo service httpd start
$ sudo chkconfig httpd on
$ cd /var/www/html
$ sudo vim index.html
<html> <h1> Welcome Linux Academy Students! Let's Test Failover Routing with Route 53 </h1></html>

```

Verify in Browser that the site is up and running.

### 2. Configure Route53

#### a. health check

In `Route53 > health check > create new health check`:

```
# Configure health check
Name:             cmcloudlab209.info
What to monitor:  Endpoint
Specify by:       IP
Protocol:         HTTP
IP:               34.201.114.239
Path:             index.html

# Get notified
Create Alarm:     No

```

#### b. dns record

```
Name:            www
Type:            A
Alias:           No
Value:           34.201.114.239

Routing Policy:          Failover
Associate Health Check:  Yes
Associate:               <Name of health check>

```

Verify in browser that new domain `www.cmcloudlab209.info` can be reached 
or via `$ dig www.cmcloudlab209.info`.

